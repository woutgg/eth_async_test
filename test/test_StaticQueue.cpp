#include <stdio.h>
#include <string.h>
#include "StaticQueue.h"

#define __ASSERT_USE_STDERR
#include <assert.h>


//NOTE: expects maxSize() == 1000 and maxElems() == 20
int main(int argc, char **argv) {
	StaticQueue sq;
	const int szMax = sq.maxSize(), elMax = sq.maxElems();

	int expSize, rv;

	const char *msg_l1 = "";
	const char *msg_l10 = "012345678";
	const char *msg_l100 = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678";
	const char msg_l1001[1001]; msg_l1001[1000] = '\0';

	//tests are built for these specific capacities
	assert(szMax == 1000);
	assert(elMax == 20);

	assert(sq.size() == 0);
	assert(sq.free() == szMax);
	assert(sq.elems() == 0);

	expSize = strlen(msg_l10) + 1;
	rv = sq.push(msg_l10);
	assert(rv == 0);
	assert(sq.size() == expSize);
	assert(sq.free() == szMax - expSize);
	assert(sq.elems() == 1);

	expSize = strlen(msg_l10) + 1 + strlen(msg_l100) + 1;
	rv = sq.push(msg_l100);
	assert(rv == 0);
	assert(sq.size() == expSize);
	assert(sq.free() == szMax - expSize);
	assert(sq.elems() == 2);

	sq.clear();
	assert(sq.size() == 0);
	assert(sq.free() == szMax);
	assert(sq.elems() == 0);

	//push 9*100 + 9*10 + 1*1 == 991; then push 1*100 and check whether it returns 10 (9 free + 1*1 + 9*10 == 100); then check free() == 0
	for (int i = 0; i < 9; ++i) assert(sq.push(msg_l100) == 0);
	for (int i = 0; i < 9; ++i) assert(sq.push(msg_l10) == 0);
	sq.push(msg_l1);
	rv = sq.push(msg_l100);
	assert(rv == 10);
	assert(sq.free() == 0);

	sq.clear();
	assert(sq.push(msg_l1001) == -1);

	for (int i = 0; i < 20; ++i) assert(sq.push(msg_l10) == 0);
	assert(sq.push(msg_l10) == -1);

	return 0;
}
