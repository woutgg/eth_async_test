#include "Flash.h"
#include "AsyncNetwork.h"

//static
AsyncNetworking& AsyncNetworking::instance() {
	static AsyncNetworking instance;
	return instance;
}


//Note: macAddress *must* be 6 bytes long
void AsyncNetworking::init(const uint8_t macAddress[], const IPAddress& ip, int port, senderCallback cb) {
	memcpy(macAddress_, macAddress, 6);
	ip_ = ip;
	port_ = port;
	cb_ = cb;

	Ethernet.initialize(const_cast<uint8_t*>(macAddress_));
	//TODO: add constant to make waiting for init optional here? (in order not to lose any log messages in 'safe' networking environments)
}


bool AsyncNetworking::process() {
	if (!ethInitialized_) {
		int initVal = Ethernet.initialized();
		switch (initVal) {
			case 1:
				Serial.print(F("Ethernet initialized - "));
				Serial.print(F("IP/netmask: ")); Serial.print(Ethernet.localIP());
				Serial.print(F("/")); Serial.print(Ethernet.subnetMask());
				Serial.print(F("; DNS: ")); Serial.print(Ethernet.dnsServerIP());
				Serial.print(F("; gateway: ")); Serial.println(Ethernet.gatewayIP());
				ethInitialized_ = true;
				break;
			case 2:
				Serial.println(F("Ethernet DHCP init failed, retrying..."));
				Ethernet.initialize(const_cast<uint8_t*>(macAddress_));
				break;
		}
	}

	if (ethInitialized_) {
		return processWithEthInited();
	} else {
		return false;
	}
}


/* PRIVATE FUNCTIONS */

AsyncNetworking::AsyncNetworking()
: port_(0), cb_(0), ethInitialized_(false), connecting_(false)
{}

bool AsyncNetworking::processWithEthInited() {
	if (Ethernet.maintainNeeded() != DHCP_CHECK_NONE) {
		Ethernet.maintainFinished(); //TODO: add a timer to check whether this keeps failing for too long
	}

	if (!client_.connected() && !connecting_) {
		client_.stop(); //TEMP
		connecting_ = true;

		Serial.print(F("Connecting to "));
		Serial.print(ip_);
		//Serial.print(SERVER_HOST);
		Serial.print(F(":")); Serial.println(port_, DEC);

		//initializeConnection does not conform to the 0=busy,1=ok,2=fail semantics
		int rv = client_.initializeConnection(ip_, port_);
		//int rv = client.initializeConnection(SERVER_HOST, SERVER_PORT);
		if (rv == 0) {
			Serial.println(F("Could not connect to server, retrying..."));
			connecting_ = false;
		}
		else { if (client_.connected()) Serial.println(F("<connected...>")); } //TEMP
	} else if (connecting_) {
		int rv = client_.connectionInitialized();
		switch (rv) {
			case 1:
				Serial.println(F("Connection established."));
				connecting_ = false;
				break;
			case 2:
				Serial.println(F("Could not establish connection."));
				connecting_ = false;
				break;
		}
	}

	if (ethInitialized_ && client_.connected()) {
		if (cb_) (*cb_)(client_);
		return true;
	} else {
		Serial.println(F("Cannot send...TODO: queue or count failed send?"));
	}

	return false;
}
