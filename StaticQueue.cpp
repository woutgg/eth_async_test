#include <string.h>
#include "StaticQueue.h"

const int StaticQueue::MAX_QUEUE_SIZE = 1024;
const int StaticQueue::MAX_QUEUE_ELEMS = 20;


StaticQueue::StaticQueue() {
	for (int i = 0; i < MAX_QUEUE_ELEMS; ++i) {	elems_[i] = -1; }
}

//returns number of dropped queued messages or -1 if message could not be queued (i.e., > MAX_QUEUE_SIZE)
int StaticQueue::push(const char* msg) {
	int needed = strlen(msg) + 1;
	int elems = elems();
	int free = MAX_QUEUE_SIZE - size();

	if (elems == MAX_QUEUE_ELEMS || needed > MAX_QUEUE_SIZE) return -1;

	int amt = 0, erased = 0;
	while (needed > free + amt) {
		amt += elemSize(0);
		erased++;
		pop();
	}

	int idx = findFreeElemIndex();
	int offs = size();
	strcpy(data_ + offs, msg);
	elems_[idx] = offs;

	return erased;
}

//get pointer to message at bottom of queue
const char *StaticQueue::getTop() const {
	int idx = findFreeElemIndex();
	if (idx == 0) return 0;

	return data_ + elems_[idx];
}

//pop message at bottom of queue
bool StaticQueue::pop() {
	int sz = size();
	if (sz == 0) return false;

	int no = elems_[1];
	if (no >= 0) {
		memmove(data_, data_ + no, sz - no);
		for (int i = 1; i <= elems(); ++i) {
			elems_[i - 1] = elems_[i] - no;
			elems_[i] = -1; //in case this is the last elem, otherwise it will be overwritten in the next iteration
		}
	} else {
		elems_[0] = -1;
	}

	return true;
}

void StaticQueue::clear() { elems_[0] = -1; }

int StaticQueue::maxSize() const { return MAX_QUEUE_SIZE; }

int StaticQueue::size() const {
	int idx = findFreeElemIndex();
	if (idx == 0) return 0;

	int len = elemSize(idx - 1);
	return elems_[idx] + len;
}

int StaticQueue::free() const {
	return MAX_QUEUE_SIZE - size();
}

int StaticQueue::maxElems() const { return MAX_QUEUE_ELEMS; }

int StaticQueue::elems() const {
	int idx = findFreeElemIndex();
	return idx >= 0 ? idx : MAX_QUEUE_ELEMS;
}


/* PRIVATE FUNCTIONS */

int StaticQueue::findFreeElemIndex() const {
	for (int i = 0; i < MAX_QUEUE_ELEMS; ++i) {
		if (elems_[i] == -1) return i;
	}
	return -1;
}

int StaticQueue::elemSize(int idx) const {
	if (elems_[idx] == -1) return -1;
	return strlen(data_ + elems_[idx]) + 1;
}
