/*
 * TODO:
 * - change eth library to return constants instead of 0/1(/2) (and how about connected/connectionInitialized?)
 *
 * Notes:
 * - To run a test server, issue `nc -kl 1234`
 */
#include "Flash.h"
#include "AsyncNetwork.h"


static const int BLINK_PIN = 9;
static const int32_t MAX_LOOP_ERROR = 100; //set to -1 to always log stats
static const unsigned int DEFAULT_BLINK_DELAY = 1000 / 1;

static const uint8_t MAC_ADDRESS[] = { 0x90, 0xA2, 0xDA, 0x0F, 0x44, 0x79 };
//static const IPAddress SERVER_ADDRESS(109, 72, 85, 116);
//static const char *SERVER_HOST = "www.lsof.nl";
//static const int SERVER_PORT = 80;
static const IPAddress SERVER_ADDRESS(192, 168, 2, 1);
static const int SERVER_PORT = 1234;


/* forward declarations */
void sendPacket(EthernetClient& client);
int freeRam();


AsyncNetworking& anw = AsyncNetworking::instance();
unsigned long blinkDelay = DEFAULT_BLINK_DELAY;
unsigned long lastLoopTime = 0;


void setup() {
	Serial.begin(115200);
	while (!Serial);

	Serial.println(F("=== Async Ethernet library test ==="));
	Serial.print(F("free ram: ")); Serial.println(freeRam());

	pinMode(BLINK_PIN, OUTPUT);
	digitalWrite(BLINK_PIN, LOW);

	Serial.println(F("Initializing ethernet..."));
	anw.init(MAC_ADDRESS, SERVER_ADDRESS, SERVER_PORT, &sendPacket);
}


void loop() {
	const unsigned long sTime = millis();

	delay(blinkDelay);
	digitalWrite(BLINK_PIN, !digitalRead(BLINK_PIN));

	const unsigned long eTime = millis();
	const unsigned long delta = eTime - lastLoopTime;
	const unsigned long err = delta - blinkDelay;

	if (MAX_LOOP_ERROR == -1 || (int32_t)err > MAX_LOOP_ERROR) {
		//Serial.print("* Loop error exceeded");
		Serial.print(F("* Loop stats"));
		//Serial.print("\tDt "); Serial.print(delta, DEC); //delta
		Serial.print(F("\tEr ")); Serial.print(err, DEC); //error
		Serial.print(F("\t\tLn ")); Serial.print(eTime - sTime, DEC); //loop duration
		Serial.print(F("\t\tTm ")); Serial.println(sTime, DEC); //now
	}

	anw.process();
	Serial.print(F("free ram: ")); Serial.println(freeRam()); //TEMP

	lastLoopTime = eTime;
}


void sendPacket(EthernetClient& client) {
	//TEST CODE
	static uint32_t counter = 0;
	Serial.println(F("Sending stuff"));
	client.write("ping#"); client.print(counter, DEC); client.write(" ");
	counter++;
}

int freeRam() {
	extern int __heap_start, *__brkval;
	int v;
	return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}
