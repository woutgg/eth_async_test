/*
 * Quick & dirty class to abstract networking away from main code.
 * Call init() once and then process() repeatedly.
 * Arguments to init should be mostly self-explanatory; the callback will be called
 * by process() each time it is called and has an open connection to the server specified
 * to init(). As long as this connection is not open, it will keep attempting to open one.
 * The callback gets an argument of EthernetClient& which can be used to write packet data.
 */
#ifndef ASYNC_NETWORKING_H_SEEN
#define ASYNC_NETWORKING_H_SEEN

#include "SPI.h"
#include "Ethernet_nb.h"

class AsyncNetworking {
public:
	typedef void (*senderCallback)(EthernetClient&);

	static AsyncNetworking& instance();

	void init(const uint8_t macAddress[], const IPAddress& ip, int port, senderCallback cb = 0);
	bool process();


	//TODO: this function should send given data if a connection is available.
	// otherwise it should add it (as a separate 'message/packet') to a queue
	// and this queue should be limited in size to prevent heap overflow
	//If queueing is not feasible, keep a failure counter so we can at least detect dropped data
	void queueData(const char *data);


private:
	uint8_t macAddress_[6];
	IPAddress ip_;
	int port_;
	senderCallback cb_;

	EthernetClient client_;
	bool ethInitialized_;
	//bool rqInProgress_;
	bool connecting_;

	AsyncNetworking();
	AsyncNetworking(const AsyncNetworking& o);
	AsyncNetworking& operator=(AsyncNetworking& o);

	bool processWithEthInited();
};

#endif /* !ASYNC_NETWORKING_H_SEEN */
