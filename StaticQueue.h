/*
 * A message queue which does not use dynamic memory allocation.
 * push() attempts to add a new message, dropping as many older messages as neccesary.
 * getTop() returns a pointer to the oldest message present.
 * pop() drops the oldest message.
 *
 * Use with AsyncNetworking:
 * - sendMsg(msg): if connection available, process current queue, then send msg; otherwise queue msg and increase packet_drop_count with return value
 * - processing the queue means: while(elems() > 0) { send(getTop()); pop(); }
 *
 * TODO:
 * - add <Type[dataCap], ElemStore[elemCap]> as template arguments to make capacities compile-time dynamic?
 */

class StaticQueue {
public:
	StaticQueue();

	//returns number of dropped queued messages or -1 if message could not be queued (i.e., > MAX_QUEUE_SIZE)
	int push(const char* msg);

	//get pointer to message at bottom of queue
	const char *getTop() const;

	//pop message at bottom of queue
	bool pop();

	void clear();
	int maxSize() const;
	int size() const;
	int free() const;
	int maxElems() const;
	int elems() const;

private:
	static const int MAX_QUEUE_SIZE;
	static const int MAX_QUEUE_ELEMS;

	char data_[MAX_QUEUE_SIZE];
	int elems_[MAX_QUEUE_ELEMS];

	int findFreeElemIndex() const;
	int elemSize(int idx) const;
};
